'use strict';
console.log('Loading function');
const AWS = require('aws-sdk');
const sesClient = new AWS.SES();
const contactForm = require('./src/ContactForm');

/**
 * Lambda to process HTTP POST for contact form with the following body
 * {
      "email": <contact-email>,
      "subject": <contact-subject>,
      "message": <contact-message>
    }
 *
 */
exports.handler = (event, context, callback) => {
    console.log('Received event:', JSON.stringify(event, null, 2));
    try {
        let contact = new contactForm(sesClient);
        contact.send(event, callback);
    } catch (err) {
        console.log('Email sending fail: ',err);
    }
};

function getEmailMessage (emailObj) {
    try {
    var emailRequestParams = {
        Destination: {
          ToAddresses: [ emailObj.email ]  
        },
        Message: {
            Body: {
                Text: {
                    Data: emailObj.message
                }
            },
            Subject: {
                Data: ' We will reply to ' + emailObj.name + ' soon.'
            }
        },
        Source: sesConfirmedAddress,
        ReplyToAddresses: [ sesConfirmedAddress ]
    };
    } catch (err) {
        console.log('Email params fail: ',err);
    }
    
    return emailRequestParams;
}