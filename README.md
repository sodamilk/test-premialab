# Serverless sample for PremiaLab

I build with NodeJS with Serverless + AWS Lambda.
Besides, I use Bootstrap template as 2 pages website sample.

## What I did
* NodeJS express Router for / and /portfolio
* AWS Lambda Function - website
* Aws Lambda Function - contcatForm

## Additional
* Serverless + S3 bucket on static files. such as js, css
* Jest : testing for class ContactForm getMessage()
* Enable AWS Simple Email Service

## Not yet complete
* Contact form submitting is not AJAX


## Instruction
* Website - after loading into frontpage, you can switch to Portfolio page by Navigation at right hand side
* AWS Lambda Function API - POST request sample
```
{
    "email": "sodamilkhk@gmail.com"
    , "name": "test"
    , "message": "contenr"
}
```

## Reference
* [Website](https://r5mebb4et5.execute-api.us-east-1.amazonaws.com/dev/) - https://r5mebb4et5.execute-api.us-east-1.amazonaws.com/dev/
* [Coding](https://bitbucket.org/sodamilk/test-premialab/src/master/) - https://bitbucket.org/sodamilk/test-premialab/src/master/

## Authors

* **Yan Leung** - *NodeJS sample*
