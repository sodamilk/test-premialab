'use strict';
//import { contactForm } from "../src/contactForm";
const AWS = require('aws-sdk');
const sesClient = new AWS.SES();
const contactForm = require('../src/ContactForm');

test('test getMessage()', () => {
    const emailObj = {
        "email": "sodamilkhk@gmail.com"
        , "name": "test"
        , "message": "contenr"
    };

    let contact = new contactForm(sesClient);
    const result = contact.getEmailMessage(emailObj);

    expect(result.Destination.ToAddresses).toContain(emailObj.email);
});

// test('test send()', () => {
//     const request =     {
//         "email": "sodamilkhk@gmail.com"
//         , "name": "test"
//         , "message": "contenr"
//     }       
//     const event = {
//         body: JSON.stringify(request)
//     }

//     let contact = new contactForm(sesClient);
//     const result = contact.send(event);

//     expect(result.Destination.ToAddresses).toContain(emailObj.email);
// });
