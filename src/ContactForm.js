class ContactForm {
    constructor(sesClient) {
        this.sesClient = sesClient;
        this.response = {
            statusCode: 200,
            body: null
        }
		this.sesConfirmedAddress = "sodamilkhk@gmail.com";
    }

    send(event) {
        var emailObj = JSON.parse(event.body);
        var params = this.getEmailMessage(emailObj);
        this.sendEmail(params);
    }

    sendEmail(params, callback) {
        var sendEmailPromise = this.sesClient.sendEmail(params)
            .promise();
        var _this = this;
        sendEmailPromise
            .then(function(result) {
                console.log(result);
                _this.response.body = JSON.stringify({
                    "message": "Email is sent."
                });
                callback(null, _this.response);
            })
            .catch(function(err) {
                console.log(err);
                callback(null, err);
            });
    }

    getEmailMessage(emailObj) {
        try {
            var emailRequestParams = {
                Destination: {
                    ToAddresses: [emailObj.email]
                }
                , Message: {
                    Body: {
                        Text: {
                            Data: emailObj.message
                        }
                    }
                    , Subject: {
                        Data: ' We will reply to ' + emailObj.name + ' soon.'
                    }
                }
                , Source: this.sesConfirmedAddress
                , ReplyToAddresses: [this.sesConfirmedAddress]
            };
        } catch (err) {

        }

        return emailRequestParams;
    }

}

module.exports = ContactForm;