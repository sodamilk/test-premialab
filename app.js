'use strict'
const path = require('path')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const compression = require('compression')
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')
const app = express()
const router = express.Router()

app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);

router.use(cors())
router.use(bodyParser.json())
router.use(bodyParser.urlencoded({ extended: true }))
router.use(awsServerlessExpressMiddleware.eventContext())

app.set('views', path.join(__dirname, 'views'))

router.get('/', (req, res) => {
  res.render('index')
})

router.get('/', (req, res) => {
  res.render('index')
})

router.get('/portfolio', (req, res) => {
  res.render('portfolio')
})


app.use('/static/vendor', express.static('static/vendor'));
app.use('/static/css', express.static('static/css'));
app.use('/static/js', express.static('static/js'));
app.use('/static/img', express.static('static/img'));
app.use('/', router)

module.exports = app